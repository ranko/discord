CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended Modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Discord module connects your drupal site with Discord. It enables 
real-time messaging, archiving and search for modern teams, and it has cool
system integrations features.

This module allows you to send messages from a Drupal website to Discord.
It has Rules module integration, and you can use the module's API in your
modules.

 * For a full description of the module visit:
   https://www.drupal.org/project/discord

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/discord


REQUIREMENTS
------------

This module requires the following outside of Drupal core:

 * Discord account and admin access to a Discord server.

A webhook integration is required.
    1. Navigate to your server (do not use the mobile app).
    2. Open Server Settings -> Integrations -> Create Webhook
    3. Set the default name, avatar and channel where the messages will arrive.
    4. Copy the webhook URL by using the provided button. 

For more information see:
 * https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks
 * https://discord.com/developers/docs/resources/webhook


RECOMMENDED MODULES
-------------------

To enable the Rules-based Discord integration:

 * Rules - https://www.drupal.org/project/rules

Useful links (about the Rules module):

 * https://www.drupal.org/documentation/modules/rules
 * https://fago.gitbooks.io/rules-docs/content/


INSTALLATION
------------

 * Install the Discord module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    3. Navigate to Administration > Configuration > Web Services > Discord >
       Configuration to configure the Discord  module.
    3. Enter the Webhook URL that was obtained from the server settings.
    4. Enter the default user name that you would like to see in your channel.
    5. Save configuration.
    9. To test the messaging system, navigate to Administration > Configuration
       > Web Services > Discord > Send a test message. Enter a message and select
       "Send message." The message should be sent to the selected Discord channel.


MAINTAINERS
-----------

 * Ranko Marinić - https://www.drupal.org/u/ranko

Supporting organization:

 * ADCI Solutions - https://www.drupal.org/adci-solutions
