<?php

namespace Drupal\discord\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\discord\Discord;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class SendTestMessageForm.
 *
 * @package Drupal\discord\Form
 */
class SendTestMessageForm extends FormBase {

  /**
   * Discord service.
   *
   * @var \Drupal\discord\Discord
   */
  protected $discordService;

  /**
   * {@inheritdoc}
   */
  public function __construct(Discord $discord) {
    $this->discordService = $discord;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('discord.discord_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'discord_send_test_message';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('discord.settings');
    $form['discord_test_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#required' => TRUE,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send message'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($this->config('discord.settings')->get('discord_webhook_url'))) {
      $form_state->setRedirect('discord.admin_settings');
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getRedirect())) {
      $config = $this->config('discord.settings');
      $message = $form_state->getValue('discord_test_message');
      $username = $config->get('discord_username');
      $avatar = $config->get('discord_avatar_url');
      $response = $this->discordService->sendMessage($message, $username, $avatar);
      if ($response && RedirectResponse::HTTP_OK == $response->getStatusCode()) {
        $this->messenger()->addMessage($this->t('Message was successfully sent!'));
      }
      else {
        $this->messenger()->addWarning($this->t('Please check log messages for further details'));
      }
    }
  }

}
