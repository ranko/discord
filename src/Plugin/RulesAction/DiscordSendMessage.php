<?php

namespace Drupal\discord\Plugin\RulesAction;

use Drupal\discord\Discord;
use Drupal\rules\Core\RulesActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a 'Discord send message' action.
 *
 * @RulesAction(
 *   id = "rules_discord_send_message",
 *   label = @Translation("Send message to Discord"),
 *   category = @Translation("System"),
 *   context = {
 *     "message" = @ContextDefinition("string",
 *       label = @Translation("Content"), 
 *       description = @Translation("Specify the content of the message which will be sent to Discord. You can use markdown to format the message. Markdown can be used to format the message."),
 *     ),
 *     "username" = @ContextDefinition("string",
 *       label = @Translation("Username"),
 *       description = @Translation("Override the default username."),
 *       default_value = NULL,
 *       required = FALSE,
 *     ),
 *     "avatar_url" = @ContextDefinition("string",
 *       label = @Translation("Avatar URL"),
 *       description = @Translation("Set a full path to an image to override the default avatar."),
 *       default_value = NULL,
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class DiscordSendMessage extends RulesActionBase implements ContainerFactoryPluginInterface {

  /**
   * Discord service.
   *
   * @var \Drupal\discord\Discord
   */
  protected $discordService;

  /**
   * Constructs a DiscordSendMessage object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\discord\Discord $discord_service
   *   The Discord manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Discord $discord_service) {
    $this->discordService = $discord_service;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\discord\Discord $discord_service */
    $discord_service = $container->get('discord.discord_service');
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $discord_service
    );
  }

  /**
   * Send message to discord.
   *
   * @param string $message
   *   The message to be sent.
   * @param string $username
   *   The Discord username.
   * @param string $avatar
   *   The Discord avatar URL.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function doExecute($message, $username = '', $avatar = '') {
    $this->discordService->sendMessage($message, $username, $avatar);
  }

}
